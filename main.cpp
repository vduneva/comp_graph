#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

GLint Wf = 640, Hf = 640;
GLfloat eyex = 2.0, eyey = 1.0, eyez = 2.0;
GLfloat ratio = Wf / (GLfloat) Hf;

GLfloat mat0_ambient[] = { 0.5f, 0.5f, 0.7f, 1.0f };
GLfloat mat0_diffuse[] = { 0.0f, 0.5f, 0.0f, 1.0f };
GLfloat mat0_specular[] = { 1.0f, 0.5f, 1.0f, 1.0f };
GLfloat mat0_shininess[] = { 20.0f };

void init()
{
glEnable( GL_LIGHTING );
glEnable( GL_LIGHT0 );
glEnable( GL_LIGHT1);

glShadeModel( GL_SMOOTH );
glEnable( GL_DEPTH_TEST );
glEnable( GL_NORMALIZE );
glClearColor( 0.5f, 0.5f, 0.5f, 0.0f);//grey
glViewport( 0, 0, Wf, Hf );
glEnable (GL_COLOR_MATERIAL);
glColorMaterial (GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
}

void chairLeg( GLdouble thick, GLdouble len)
{
glPushMatrix();
glTranslated( 0, len/2, 0 );
glScaled( thick, len*2, thick );
glutSolidCube( 1.0 );
glPopMatrix();
}

void chair( GLdouble topWidth, GLdouble topThickness,
GLdouble legThickness, GLdouble legLength )
{
glPushMatrix();
glTranslated( 0, legLength*1.5, 0 );
glScaled( topWidth, topThickness, topWidth );
glutSolidCube( 1.0);
glPopMatrix();

GLdouble dist = 0.95 * topWidth/2.0 - legThickness/2.0;

glPushMatrix();
glTranslated( 0.1, 0.22, 0 );
glScaled( topWidth/10, 0.2, 0.2 );
glutSolidCube( 1.0 );
glPopMatrix();

glPushMatrix();
glTranslated( dist, 0, dist );
chairLeg( legThickness, legLength );
glPopMatrix();
glPushMatrix();
glTranslated( -dist, 0, dist );
chairLeg( legThickness, legLength );
glPopMatrix();
glPushMatrix();
glTranslated( dist, 0, -dist );
chairLeg( legThickness, legLength );
glPopMatrix();
glPushMatrix();
glTranslated( -dist, 0, -dist );
chairLeg( legThickness, legLength );
glPopMatrix();
}

void setCamera()
{
//clear buffers to preset values
glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
glMatrixMode( GL_PROJECTION );
//replace the current matrix with the identity matrix
glLoadIdentity();
// set up a perspective projection matrix
gluPerspective(21.0f, ratio, 0.1f, 100.0f);
glMatrixMode( GL_MODELVIEW );
glLoadIdentity();
gluLookAt( eyex, eyey, eyez, 0, 0.090, 0, 0.0, 1.0, 0.0 );
}

void setLights()
{
GLfloat light0Position[] = { 2.0f, 6.0f, 3.0f, 1.0f };
GLfloat light0Intensity[] = { 1.0f, 1.0f, 1.0f, 1.0f };
glLightfv( GL_LIGHT0, GL_POSITION, light0Position );
glLightfv( GL_LIGHT0, GL_DIFFUSE, light0Intensity );

GLfloat light1Position[] = { -2.0f, -2.0f, 2.0f, 0.0f };
GLfloat light1Intensity[] = { 1.0f, 1.0f, 1.0f, 1.0f };
glLightfv( GL_LIGHT1, GL_POSITION, light1Position );
glLightfv( GL_LIGHT1, GL_DIFFUSE, light1Intensity );
}

void setMaterial( GLfloat *a, GLfloat *d, GLfloat *sp, GLfloat *sh )
{
glMaterialfv( GL_FRONT, GL_AMBIENT, a );
glMaterialfv( GL_FRONT, GL_DIFFUSE, d );
glMaterialfv( GL_FRONT, GL_SPECULAR, sp );
glMaterialfv( GL_FRONT, GL_SHININESS, sh );
}

void specialKeyboardFunc( int key, int x, int y )
{
GLfloat t = 0.1;
switch( key ) {
case GLUT_KEY_LEFT : eyex += -t; break;
case GLUT_KEY_RIGHT: eyex +=  t; break;
case GLUT_KEY_UP   : eyey +=  t; break;
case GLUT_KEY_DOWN : eyey += -t; break;
case GLUT_KEY_HOME : eyez +=  t; break;
case GLUT_KEY_END  : eyez += -t; break;
}
glutPostRedisplay();
}

void ball(GLfloat posX, GLfloat posY, GLfloat posZ)
{
glPushMatrix();
glTranslated(posX, posY, posZ );
glutSolidSphere( 0.04, 60, 250 );
glPopMatrix();
}

void ballon(GLfloat posX, GLfloat posY, GLfloat posZ)
{
ball(posX,posY,posZ);
glPushMatrix();
 glLineWidth( 2.0 );
 glBegin( GL_LINES );
    glVertex3f( posX, posY-0.11,posZ );
    glVertex3f( posX, posY,posZ );
 glEnd();
glPopMatrix();
}

void displayFunc()
{
setCamera();
setLights();
setMaterial( mat0_ambient, mat0_diffuse, mat0_specular, mat0_shininess );

glColor3f(0.0f, 0.5f, 0.5f);//Blue-Green
ball( -0.25, -0.42, 0.35);
glColor4f(1.0f, 1.0f, 0.0f, 0.0f);//yellow
ballon( 0.25, 0.42, 0.35 );
glColor3f(0.1f, 0.0f, 0.1f);//Dark Purple
ballon( 0.10, 0.42, 0.33 );
glColor3f(0.0f, 0.1f, 0.0f);//Forest Green
ballon( 0.5, 0.52, 0.32 );

glColor4f(1.0f, 1.0f, 1.0f, 0.0f);//white
glPushMatrix();
 glTranslated(0.4, 0, 0.4);
  chair(0.2, 0.02, 0.03, 0.1);
glPopMatrix();
glColor3f(2.0f, 0.5f, 1.0f);//Lilac
glPushMatrix();
 glTranslated( 0.4, 0.15, 0.45  );
 glRotated(-110,0.4,0.3,0.3);
 glutSolidCone( 0.04, 0.13, 40, 40 );
glPopMatrix();

//Performs a buffer swap on the layer in use for the current window
glutSwapBuffers();
}

int main(int argc, char *argv[])
{
//initialize the GLUT library.
glutInit( &argc, argv);
glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
glutInitWindowSize( Wf, Hf );
glutInitWindowPosition( 10, 10 );
glutCreateWindow( "Birthday" );
init();
glutSpecialFunc( specialKeyboardFunc );
glutDisplayFunc( displayFunc );
glutMainLoop();

return 0;
}
